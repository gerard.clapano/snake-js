// canvas declaration
let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");

// snake object declaration
let s = new Snake();
// food object declaration
let f = new Food();

// adds keyboard control 
document.body.addEventListener("keydown", function(event) {
    s.move(event)
});

// main loop logic
function run() {
    ctx.fillStyle = "#000000";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    s.update();
    s.draw();
    s.limitChecker();
    f.draw();
    f.hitChecker();

}

// loop
setInterval(run, 100);