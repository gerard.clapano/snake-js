class Food {

	constructor() {
        this.scale = 20;
        this.x = random(0, (canvas.width - this.scale) / this.scale) * this.scale;
        this.y = random(0, (canvas.height - this.scale) / this.scale) * this.scale;
	}

    // draw the current position of the food
	draw() {
		ctx.fillStyle = "yellow";
		ctx.fillRect(this.x, this.y, this.scale, this.scale);
	}

    // checks if the position of the food matches the head of the snake
	hitChecker() {
		if(s.x == this.x && s.y == this.y) {
			this.x = random(0, (canvas.width - this.scale) / this.scale) * this.scale;
            this.y = random(0, (canvas.height - this.scale) / this.scale) * this.scale;
            // console.log(`x: ${this.x} || y: ${this.y}`);
		}
	}


}

// random number generator that take 2 arguments (min, max)
function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}