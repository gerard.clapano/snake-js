class Snake {

	constructor() {
		this.x = 400;
		this.y = 200;
		this.scale = 20;
		this.xSpeed = this.scale;
        this.ySpeed = 0;
        //trial code
        this.total = 4;
	}

    // draw the current position of the snake
	draw() {
		ctx.fillStyle = "#ff9999";
		ctx.fillRect(this.x, this.y, this.scale, this.scale);
		
    }

	// this is responsible for moving the snake
	update() {
		this.x += this.xSpeed;
		this.y += this.ySpeed;
	}

	// set the direction of the snake object with the corresponding key
	move(event) {
		if(event.key == "ArrowUp") {
			this.ySpeed += -this.scale;
			this.xSpeed = 0;
		}

		if(event.key == "ArrowDown") {
			this.ySpeed += this.scale;
			this.xSpeed = 0;
		}

		if(event.key == "ArrowLeft") {
			this.xSpeed += -this.scale;
			this.ySpeed = 0;
		}

		if(event.key == "ArrowRight") {
			this.xSpeed += this.scale;
			this.ySpeed = 0;
		}
	}

    // sets the boundery of the canvas
	limitChecker() {
		if(this.x < this.scale) {
			this.x  = 0;
			this.xSpeed = 0;
		}

		if(this.x > canvas.width - this.scale) {
			this.x = canvas.width - this.scale;
			this.xSpeed = 0;
		}

		if(this.y < this.scale) {
			this.y  = 0;
			this.ySpeed = 0;
		}

		if(this.y > canvas.height - this.scale) {
			this.y = canvas.height - this.scale;
			this.ySpeed = 0;
		}
	}

}
